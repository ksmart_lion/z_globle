import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
export 'form/dropdown.dart';
export 'form/textField.dart';
export 'form/button.dart';
export 'form/step.dart';
export 'form/stepStyle2.dart';
export 'form/uploadImage.dart';
export 'form/uploadMitiImage.dart';
export 'form/tabScroll.dart';
export 'form/multiDropdown.dart';
export 'form/date.dart';
export 'form/rangeDate.dart';
export 'form/coTime.dart';
import 'g.dart' as gCo;
import 'helper/stringExtension.dart';


class FieldProperty {
  static final colors = Colors.black87;
  final fieldcolor = colors;
  static final colorFocus = Colors.blue;
  final fieldContentPadding = new EdgeInsets.only(left: 17.0, top: 39.0, bottom: 0);
  final fieldContentPaddingSmall = new EdgeInsets.fromLTRB(10.0, 23.0, 10.0, 10.0);
  final fieldStyle = new TextStyle(
      fontWeight: FontWeight.normal, fontSize: 15, color: Colors.black87);
  final fieldBorder = OutlineInputBorder(
    borderSide:
    const BorderSide(color: Colors.blue, width: 1.5),
    borderRadius: BorderRadius.circular(6),
    gapPadding: 10,
  );
  final fieldBorderError = OutlineInputBorder(
    borderSide:
    const BorderSide(color: Colors.redAccent, width: 1.5),
    borderRadius: BorderRadius.circular(6),
    gapPadding: 10,
  );
  final fieldEnabledBorder = OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.black38, width: 1),
    borderRadius: BorderRadius.circular(6),
    gapPadding: 10,
  );
  final fieldEnabledBorderError = OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.redAccent, width: 1),
    borderRadius: BorderRadius.circular(6),
    gapPadding: 10,
  );
  final fieldEnabledBorderWithValue = OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.black38, width: 1),
    borderRadius: BorderRadius.circular(6),
    gapPadding: 10,
  );
  final TextStyle fieldLabelStyle = TextStyle(
    color: Colors.black54,
    //fontWeight: FontWeight.bold,
    fontSize: 15,
  );
  final TextStyle fieldLabelHasFocusStyle = TextStyle(
    color: Colors.blue,
    //fontWeight: FontWeight.bold,
    fontSize: 19,
  );
}


/* ------------------------------------------------------------------------- */
enum coFormSize {small, normal, medium}
Map noSelectItemRow = {
  'id' : 'null',
  'name' : 'please_select_item'.translate().toFirstUppercase()
};
Future<List> addNoSelect(List list)async{
  List cList = [];
  cList.add(noSelectItemRow);
  cList.addAll(list);
  return cList;
}

void onLoading(context, {text}) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Dialog(
        backgroundColor: Colors.transparent,
        child: Container(
          decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.all(
                const Radius.circular(10.0),
              )),
          padding: EdgeInsets.symmetric(vertical: 22.0, horizontal: 5.0),
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              new Text("${text == null ? 'please_wait'.translate().toFirstUppercaseWord() : text}"),
              SizedBox(
                height: 20.0,
              ),
              new CircularProgressIndicator(),
            ],
          ),
        ),
      );
    },
  );
}

void onLoadingImage(context) {
  showGeneralDialog(
      barrierColor: Colors.black.withOpacity(0.5),
      transitionBuilder: (context, a1, a2, widget) {
        final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
        return Transform(
          transform: Matrix4.translationValues(0.0, curvedValue * 200, 0.0),
          child: Opacity(
            opacity: a1.value,
            child: Align(
              alignment: Alignment.center,
              child: Container(
                width: 30,
                height: 30,
                child: SizedBox.expand(child: CupertinoActivityIndicator()),
                margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(40),
                ),
              ),
            ),
          ),
        );
      },
      transitionDuration: Duration(milliseconds: 200),
      barrierDismissible: true,
      barrierLabel: '',
      context: context,
      pageBuilder: (context, animation1, animation2) {});
}

void onLoadingImage2(context) {
  showGeneralDialog(
    barrierLabel: "Label",
    barrierDismissible: true,
    barrierColor: Colors.black.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 200),
    context: context,
    pageBuilder: (context, anim1, anim2) {
      return Align(
        alignment: Alignment.center,
        child: Container(
          width: 30,
          height: 30,
          child: SizedBox.expand(child: CupertinoActivityIndicator()),
          margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(40),
          ),
        ),
      );
    },
    transitionBuilder: (context, anim1, anim2, child) {
      print(anim1.value);
      return SlideTransition(
        position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
        child: Opacity(opacity: anim1.value, child: child),
      );
    },
  );
}

Widget coAddNewArrow(context, {bool loaded: true}){
  return loaded == true ? Stack(children: <Widget>[
    Positioned(
      bottom: 200,
      child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Text(
                'no_item'.translate().toFirstUppercaseWord()+ ',',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black.withOpacity(0.7),
                    fontSize: 19,
                    fontWeight: FontWeight.w400),
              ),
              Text(
                'please_add_another_one'.translate().toFirstUppercase(),
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black.withOpacity(0.7),
                    fontSize: 19,
                    fontWeight: FontWeight.w400),
              ),
            ],
          )),
    ),
    Positioned(
      bottom: 80,
      right: 0,
      child: Image.asset(
        'assets/images/arrow_suggest.png',
        width: 160,
      ),
    ),
  ]) : SizedBox(width: 0,);
}

Widget coAddNewButtonWithEffect({ VoidCallback onPressed }){
  return Container(
    width: 100,
    height: 100,
    child: Stack(
      children: <Widget>[
        Positioned(
          right: -10,
          bottom: -10,
          child: Container(
            width: 100,
            height: 100,
            // color: Colors.red,
            child: SpinKitRipple(
              color: Theme.of(gCo.coContext).primaryColor,
              size: 100,
            ),
          ),
        ),
        Positioned(
          right: 10,
          bottom: 10,
          child: Container(
            height: 60,
            width: 60,
            child: new FloatingActionButton(
                backgroundColor: Theme.of(gCo.coContext).primaryColor,
                child:new Icon(Icons.add),
                onPressed: () => onPressed()
            ),
          ),
        ),
      ],
    ),
  );
}