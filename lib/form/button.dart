import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import '../g_widget.dart';

/* ------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------- */
class coButton extends StatefulWidget {
  String text;
  GestureTapCallback onPressed;
  var minWidth;
  double height;
  Color color;
  double radius;
  Icon icon;
  TextStyle textStyle;
  coButtonSize buttonSize;
  bool transparent;

  coButton(
      {Key key,
        this.text,
        this.onPressed,
        this.minWidth : -1,
        this.height,
        this.color,
        this.transparent : false,
        this.textStyle,
        this.radius: 4,
        this.buttonSize : coButtonSize.big,
        this.icon})
      : super(key: key);

  @override
  _coButtonState createState() => _coButtonState();
}

class _coButtonState extends State<coButton> {
  @override
  Widget build(BuildContext context) {
    var _with = widget.minWidth;
    if (_with < 0) {
      _with = MediaQuery.of(context).size.width * 0.9;
    }
    return Container(
      child: ButtonTheme(
        minWidth: _with,
        height: widget.height == null ? widget.buttonSize == coButtonSize.big ? 50 : 40 : widget.height,
        child: widget.icon == null
            ? new FlatButton(
          shape: widget.transparent ? RoundedRectangleBorder(side: BorderSide(
              color: widget.color == null
                  ? Theme.of(context).primaryColor
                  : widget.color,
              width: 1,
              style: BorderStyle.solid
          ), borderRadius: BorderRadius.circular(widget.radius)) : new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(widget.radius),
          ),
          color: widget.transparent ? Colors.white :(widget.color == null
              ? Theme.of(context).primaryColor
              : widget.color),
          textColor: widget.transparent == false ? Colors.white : Colors.black,
          //padding: EdgeInsets.all(8.0),
          splashColor: Colors.blueAccent,
          child: Text(
            widget.text == null ? 'Submit' : '${widget.text}',
            style: widget.textStyle == null
                ? TextStyle(fontSize: widget.buttonSize == coButtonSize.big ? 16 : (widget.buttonSize == coButtonSize.medium ? 14 : 12))
                : widget.textStyle,
          ),
          onPressed: () {
            if (widget.onPressed != null) {
              widget.onPressed();
            }
          },
        )
            : new FlatButton.icon(
          icon: widget.icon,
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(widget.radius),
          ),
          color: widget.color == null
              ? Theme.of(context).primaryColor
              : widget.color,
          textColor: widget.transparent == false ? Colors.white : Colors.black,
          //padding: EdgeInsets.all(8.0),
          splashColor: Colors.blueAccent,
          label: Text(
            widget.text == null ? 'Submit' : '${widget.text}',
            style: widget.textStyle == null
                ? TextStyle(fontSize: 16)
                : widget.textStyle,
          ),
          onPressed: () {
            if (widget.onPressed != null) {
              widget.onPressed();
            }
          },
        )
      ),
    );
  }
}