import 'package:flutter/material.dart';

class FormTabScrollModel {
  String id;
  String title;
  String key;
  GlobalKey keyContentItem;
  GlobalKey keyTabItem;
  Widget body;
  FormTabScrollModel(
      {this.id, this.title, this.key, this.keyContentItem, this.keyTabItem, this.body});
  FormTabScrollModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'];
    key = json['title'];
    keyContentItem = GlobalKey();
    keyTabItem = GlobalKey();
    body = json['body'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['key'] = this.key;
    data['keyContentItem'] = this.keyContentItem;
    data['keyTabItem'] = this.keyTabItem;
    data['body'] = this.body;
    return data;
  }
}
