export 'widget/scroll.dart';
export 'widget/modalBottomSheet.dart';
export 'widget/Scaffold.dart';
export 'widget/fliter.dart';
export 'widget/debugDrawer.dart';
export 'widget/webView.dart';
export 'widget/bottomNavigationBar.dart';
export 'widget/imageGalleryWithGrid.dart';
export 'widget/renderItemsWidget.dart';
//export 'widget/imageViewer.dart';
import 'widget/imageViewer.dart';
import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'g.dart' as gCo;
import 'g_widget.dart' as gWidget;
import 'helper/stringExtension.dart';

enum coButtonSize {big, medium, small}

void showSMSFailValidation() {
  coShowMessageDialog(
      title: 'validation_failed'.translate().toFirstUppercase(),
      message: 'please_input_required_fields'.translate().toFirstUppercase(),
      messageDialogType: MessageDialogType.fail);
}
void showSMSSomeThingWrong({String title, String message}) {
  coShowMessageDialog(
      title: title == null ? 'something_wrong'.translate().toFirstUppercase() : title,
      message: message == null ? 'data_response_error'.translate().toFirstUppercaseWord() : message,
      messageDialogType: MessageDialogType.fail);
}

Widget someThingWrongWidget(context, {String title}){
  return Container(
    width: MediaQuery.of(context).size.width,
    child: Center(
      child: Container(
        height: 200,
        child: Column(
          children: [
            Icon(Icons.wifi_off, size: 30,),
            SizedBox(height: 10,),
            Text(title == null ? 'no_internet_connection'.translate().toFirstUppercaseWord() : title, style: TextStyle(fontSize: 19, fontWeight: FontWeight.w400),),
          ],
        ),
      ),
    ),
  );
}

Widget debugWidget(context,{String output}){
  if(gCo.debug == false){
    return coZero();
  }
  return Column(
    children: <Widget>[
      Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        //width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.purple, width: 1)
        ),
        margin: EdgeInsets.only(top: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(child: Text('Output')),
            SizedBox(height: 10,),
            output != '' ? Container(
              //width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              color: Colors.indigo,
              child: Text(
                gCo.parseHtmlString(gCo.getPrettyJSONString(output.toString())),
                style: TextStyle(color: Colors.cyanAccent),
              ),
            ) : coZero(),
          ],
        ),
      ),
    ],
  );
}

void imageViewer(context, {String url}){
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => ImageViewer(url: url)),
  );
}

Widget debugFooterWidget(context, {List<Map<String,String>> output}){
  if(gCo.debug == false){
    return coZero();
  }
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
    width: MediaQuery.of(context).size.width,
    /*decoration: BoxDecoration(
      border: Border.all(color: Colors.white, width: 1)
    ),*/
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        /*Center(child: Text('Debug Output', style: TextStyle(fontSize: 13),)),
        SizedBox(height: 8,),*/
        Expanded(
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            color: Colors.indigo,
            child: gWidget.CoScroll(
              child: Column(
                children: List.generate(output != null ? output.length : 0, (index){
                  return  Container(
                    padding: EdgeInsets.only(bottom: 15),
                    margin: EdgeInsets.only(bottom: 20),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.cyanAccent))
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        gCo.checkKeyMap(output[index], 'note') ? (output[index]['note'] == '' || output[index]['note'] == 'null' || output[index]['note'] == null ? coZero() :
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Text(
                            'NOTE: ${output[index]['note']}',
                            style: TextStyle(color: Colors.cyanAccent),
                          ),
                        )) : coZero(),
                        gCo.checkKeyMap(output[index], 'fields') ? (
                            output[index]['fields'] != null && output[index]['fields'] != 'null' && output[index]['fields'] != '' ? (
                                output[index]['fields'].length > 0 ?
                                Text(
                                  gCo.checkKeyMap(output[index], 'output') ? gCo.parseHtmlString(gCo.getPrettyJSONString(output[index]['fields'], title: 'fields'.toUpperCase())) : '',
                                  style: TextStyle(color: Colors.cyanAccent),
                                )
                                : coZero()) : coZero()
                        ) : coZero(),
                        Text(
                            gCo.checkKeyMap(output[index], 'output') ? gCo.parseHtmlString(gCo.getPrettyJSONString(output[index]['output'])) : '',
                          style: TextStyle(color: Colors.cyanAccent),
                        ),
                      ],
                    ),
                  );
                }),
              ),
              /*new Text(
                gCo.parseHtmlString(gCo.getPrettyJSONString(output.toString())),
                style: TextStyle(color: Colors.cyanAccent),
              )*/
            ),
          ),
        ),
      ],
    ),
  );
}

coDialog(BuildContext context, {String title : 'Title', Widget build, Color headColor, height : 450.0}){
    headColor = (headColor == null ? Theme.of(context).primaryColor : headColor);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          child: Container(
            height: height,
            color: Colors.transparent,
            child: Column(
              children: <Widget>[
                Container(
                  decoration: new BoxDecoration(
                    color: headColor,
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(10.0),
                        topRight: const Radius.circular(10.0)),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Center(
                    child: Text(
                      '${title}',
                      style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold, fontSize: 17),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.only(
                        bottomLeft: const Radius.circular(10.0),
                        bottomRight: const Radius.circular(10.0)),

                  ),
                  child: build != null ? build : coZero()
                ),
              ],
            ),
          ),
        ) ;
      },
    );

//    showDialog(
//        context: context,
//        child:  Theme(
//          data: Theme.of(context).copyWith(dialogBackgroundColor: Colors.orange),
//          child: CupertinoAlertDialog(
//            title: Text("Please Input"),
//            content: Material(
//              child: TextField(
//                cursorColor: Colors.blue,
//                decoration: new InputDecoration(
//                  border: new OutlineInputBorder(
//                    borderRadius: new BorderRadius.circular(5.0),
//                    borderSide: new BorderSide(color: Colors.blue),
//                  ),
//                  hintText: 'Phone or Nrc Number',
//
//                ),
//              ),
//            ),
//
//            actions: <Widget>[
//              CupertinoDialogAction(
//                  isDefaultAction: true,
//                  onPressed: (){
//                    Navigator.pop(context);
//                  },
//                  child: Text("OK")
//              ),
//
//            ],
//          ),
//        ));

}

coShowConfirm({context, title, Widget buildContent ,String content, void coYesFunction(), void coNoFunction(), yesIsDestructiveAction: true,}){
   return  showDialog(
    context: context,
    builder: (context) => new CupertinoAlertDialog(
      title: new Text('${title == null ? 'are_you_sure'.translate().toFirstUppercase() : title}?',textAlign: TextAlign.center, style: TextStyle(color: Colors.black)),
      content: buildContent == null ? new Text('${content == null ? 'you_want_to_delete_this_item'.translate().toFirstUppercase() : content}',textAlign: TextAlign.center, style: TextStyle(color: Colors.black)): buildContent,
      actions: <Widget>[
        new CupertinoDialogAction(
          isDestructiveAction: yesIsDestructiveAction,
          onPressed: () async {
            Navigator.of(context).pop(true);
            return coYesFunction();
          },
          child: new Text('yes'.translate().toFirstUppercase()),
        ),
        new CupertinoDialogAction(
          onPressed: () {
            Navigator.of(context).pop(false);

          },
          child: new Text('no'.translate().toFirstUppercase()),
        ),
      ],
    ),
  );
}

enum MessageDialogType{fail, warning, successful}

Widget coZero(){
  return SizedBox(width: 0, height: 0,);
}

void coShowMessageDialog({String title, message, MessageDialogType messageDialogType : MessageDialogType.successful, int durationInSeconds : 3}){
  final context = gCo.coContext;

  Color color = Colors.green;
  IconData icon = Icons.check_circle_outline;
  if(messageDialogType == MessageDialogType.fail){
    color = Colors.redAccent;
    icon = Icons.close;
  } else if(messageDialogType == MessageDialogType.warning){
    color = Colors.orange;
    icon = Icons.info_outline;
  }

  Flushbar(
    flushbarPosition: FlushbarPosition.TOP,
    //flushbarStyle: FlushbarStyle.GROUNDED,
    titleText: Text(title == null ? 'undo_successful'.translate().toFirstUppercaseWord() : title),
    messageText: Text(message == null ? 'item_is_already_undo'.translate().toFirstUppercaseWord() : message),
    icon: Icon(
      icon,
      size: 28.0,
      color: color,
    ),
    duration: Duration(seconds: durationInSeconds),
    animationDuration: Duration(milliseconds: 500),
    //leftBarIndicatorColor: Colors.green[300],
    margin: EdgeInsets.all(8),
    borderRadius: 8,
    backgroundColor: Colors.white,
    borderColor: color,
    boxShadows: [
      BoxShadow(color: Colors.blue[800],
        offset: Offset(0.0, 2.0),
        blurRadius: 3.0,)
    ],
  )
    ..show(context);
}

Widget loadingWidget(context, {coLength : 10}){
  return Column(
      children: List.generate( coLength, (index) {
        return  Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: Theme.of(context).hintColor.withOpacity(0.15),
                  offset: Offset(0, 3),
                  blurRadius: 10)
            ],
          ),
          margin: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
          child: Shimmer.fromColors(
            period: Duration(milliseconds: 700),
            baseColor: Colors.grey[300],
            highlightColor: Colors.grey[100],
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                          children: <Widget>[
                            Container(color: Colors.black, height: 10,),
                            SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            SizedBox(height: 10,),
                          ]
                      ),
                    ),
                    SizedBox(width: 20,),
                    Expanded(
                      child: Column(
                          children: <Widget>[
                            Container(color: Colors.black, height: 10,),
                            SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            SizedBox(height: 10,),
                          ]
                      ),
                    ),
                  ],
                )
            )
            ,
          ),
        );
      })
  );
}

void coAction(context,{String title, List<Widget> actions} ){
  /*CupertinoActionSheetAction(
    child: Text("Disable"),
    isDestructiveAction: true,
    onPressed: () {
      Navigator.pop(context);

      });*/
  final action = CupertinoActionSheet(
    title: Text(
      title == null ? 'action'.translate().toFirstUppercase() : title,
      style: TextStyle(fontSize: 20),
    ),
    message: Column(
      children: <Widget>[
        Text(
          "click_any_action".translate().toFirstUppercase(),
          style: TextStyle(fontSize: 15.0),
        ),
      ],
    ),
    actions: actions,
    /// close
    cancelButton: CupertinoActionSheetAction(
      child: Text("close".translate().toFirstUppercase()),
      onPressed: () {
        Navigator.pop(context);
      },
    ),
  );
  showCupertinoModalPopup(
      context: context, builder: (context) => action);
}

Widget imageUrlWidget(imageUrl, { BoxFit fit : BoxFit.cover, String errorImageAsset}){
  if(errorImageAsset == null){
    errorImageAsset = gCo.defaultImageAsset;
  }
  if(imageUrl == null || imageUrl == 'null' || imageUrl == '' ||  imageUrl == '${gCo.baseUrlImg}null' ||  imageUrl == '${gCo.baseUrlImg}'){
    return Container(
      child: Image.asset(
          '${errorImageAsset}',
          fit: fit,
      ),
    );
  }
  return CachedNetworkImage(
    placeholder: (context, url) {
      return Shimmer.fromColors(
        period: Duration(milliseconds: 700),
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: Container(
          color: Colors.white,
        ),
      );
    },
    imageUrl: '${imageUrl}',
    fit: fit,
    errorWidget: (context, url, error) => new Image.asset(
      '${errorImageAsset}',
      fit: fit,
    ),
  );
}