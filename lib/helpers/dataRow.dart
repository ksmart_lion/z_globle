import '../g.dart' as gCo;
import '../g_widget.dart' as gWidget;
import 'dart:convert';
class CoDataRow{
  dynamic list = [];
  dynamic data;
  int page = 1;
  bool lordMore = true;
  bool noMore = false;
  bool onLoading = false;
  bool onSearching = false;
  int numberRow;
  bool someThingWrong = false;

  Future getDataRowSetState(setState, {bool refresh : false, String url: '',  String where: '', Map<String, dynamic> fields : null,Future prepareData(dataList), showSomeThingWrong : false}) async {
    setState(() {
      someThingWrong = false;
    });
    if (refresh == true) {
      setState(() {
        lordMore = true;
        noMore = false;
        list = [];
        numberRow = 0;
      });
    }
    if (lordMore == true) {
      setState(() {
        onLoading = true;
        if(refresh == true){
          onSearching = true;
        }
        lordMore = false;
      });
      if (refresh == true) {
        page = 1;
      }
      String coUrl = '${url}?page=${page}${where}';
      if(gCo.debug){
        print('============url==============${coUrl}');
      }
      await gCo.get(url: coUrl, fields: fields).then((res) async {
        if(gCo.debug){
          print('============res==============${res.toString()}');
        }
        //gCo.printDebugScope(output: res.toString());
        try {
          data = json.decode(res);
          //print('========= Data =====${data}');
          Map dataList = await prepareData(data);
          setState(() {
            lordMore = true;
            page++;
            if( dataList['list'] == null){
              noMore = true;
              lordMore = false;
              noMore = true;
            } else {
              if(dataList['list'].length == 0){
                noMore = true;
                lordMore = false;
                noMore = true;
              }
            }
            if(dataList['list'].length <= 5){
              noMore = true;
            }
            if (list.length > 0 && refresh == false) {
              list.addAll(dataList['list']);
            } else {
              list = dataList['list'];
            }
            //print('==========dataList======${dataList}=========');
            numberRow = dataList['total'];
          });
        } catch(e) {
          if(gCo.debug){
            print('============e==============${e.toString()}');
          }
        }
          //print('==========getDataRowSetState======${list}=========');
        setState(() {
          onLoading = false;
          onSearching = false;
        });
      }).catchError((e){
        setState(() {
          someThingWrong = true;
          lordMore = false;
          onLoading = false;
          onSearching = false;
        });
        //gCo.printDebugScope( output: e.toString(), note: url, fields: fields);
        if(showSomeThingWrong){
          gWidget.showSMSSomeThingWrong();
        }
      });
    }
  }
}