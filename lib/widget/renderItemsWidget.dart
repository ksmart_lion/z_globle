import 'package:flutter/material.dart';

Widget renderItems({List list, int index, String id, ValueChanged<String> onChanged}){
  return RadioListTile(
    title: Text(
      '${list[index]['name']}',
      style: TextStyle(color: Colors.black87),
    ),
    value: list[index]['id'].toString(),
    groupValue: id.toString(),
    onChanged: onChanged,
  );
}