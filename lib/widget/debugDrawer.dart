import 'package:flutter/material.dart';
//import '../scope/ScopeManage.dart';
//import 'package:scoped_model/scoped_model.dart';
import '../g_widget.dart' as gWidget;
import '../g_form.dart' as gForm;

class DebugDrawer extends StatefulWidget {
  @override
  _DebugDrawerState createState() => _DebugDrawerState();
}

class _DebugDrawerState extends State<DebugDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        child: gWidget.CoScroll(
          child: gWidget.debugWidget(context, output: ''),
          //child: gWidget.debugWidget(context, output: '${appModel.outputDebug}'),
        ),
      ),
    );
  }
}
