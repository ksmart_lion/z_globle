import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

import '../g.dart' as gCo;
import '../g_widget.dart' as gWidget;
import '../g_form.dart' as gForm;
import '../g_model.dart';
import 'imageGallery.dart';


class CoImageGalleryWithGridView extends StatefulWidget {
  List<CoImageGalleryModel> items;
  int crossAxisCount;
  double childAspectRatio;
  double spacing;
  double borderRadius;
  BoxFit fit;

  CoImageGalleryWithGridView({
    this.items,
    this.crossAxisCount : 3,
    this.childAspectRatio : 1.0,
    this.fit : BoxFit.cover,
    this.spacing : 9,
    this.borderRadius : 4
  });

  @override
  _CoImageGalleryWithGridViewState createState() => _CoImageGalleryWithGridViewState();
}

class _CoImageGalleryWithGridViewState extends State<CoImageGalleryWithGridView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: (widget.items == null ? false : (widget.items.length > 0)) ? GridView.count(
        crossAxisCount: widget.crossAxisCount,
        shrinkWrap: true,
        crossAxisSpacing: widget.spacing,
        mainAxisSpacing: widget.spacing,
        childAspectRatio: widget.childAspectRatio,
        physics: NeverScrollableScrollPhysics(),
        children: List.generate(widget.items.length, (index){
          return GestureDetector(
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CoImageGallery(initialIndex: index, items: widget.items,)
                ),
              );
              //Navigator.of(context).pushNamed('/ImageViewer', arguments: '${gCo.baseUrlImg}${scopeV.itemModel.shopInfo[index].attachment}');
            },
            child: Hero(
              tag: 'coHeroPhotoViewGallery${index}',
              child: ClipRRect(
                borderRadius: BorderRadius.circular(widget.borderRadius),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                  ),
                  child: gWidget.imageUrlWidget(
                      '${widget.items[index].url}',
                      fit: widget.fit
                  ),
                ),
              ),
            ),
          );
        }),
      ) : Text('No Image'),
    );
  }
}

