import 'package:flutter/material.dart';
import '../g_widget.dart' as gWidget;
import '../g_form.dart' as gForm;
import '../g.dart' as gCo;
import 'package:webview_flutter/webview_flutter.dart';

class CoWebView extends StatefulWidget {
  String url;
  CoWebView({this.url : 'https://www.google.com'});
  @override
  _WebViewState createState() => _WebViewState();
}

class _WebViewState extends State<CoWebView> {
  bool showLoading = false;
  /*FlutterWebviewPlugin flutterWebviewPlugin;


  @override
  void initState() {
    super.initState();
    flutterWebviewPlugin = FlutterWebviewPlugin();

    //initialChild
    flutterWebviewPlugin.onStateChanged.listen((state) {
      //print('_RegisterScreenState.initState  state = ${state.type}');
      if (state.type == WebViewState.shouldStart) {
        setState(() {
          showLoading = true;
        });
      } else if (state.type == WebViewState.finishLoad ||
          state.type == WebViewState.abortLoad) {
        setState(() {
          showLoading = false;
        });
      }
    });
  }*/

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
        child: Stack(
          children: [
            WebView(
            initialUrl: widget.url,
            javascriptMode: JavascriptMode.unrestricted,
            onPageFinished: (String url) {
              setState(() {
                showLoading = true;
              });
              //print('Page finished loading: $url');
            },
            ),
            !showLoading ?
                Positioned(
                    child: gCo.loadingWidget()
                ) : gWidget.coZero()
          ],
        ),
        /*WebviewScaffold(
          url: widget.url,
          hidden: true,
          initialChild: gCo.loadingWidget(),
          withZoom: true,
          withLocalStorage: true,
        ),
      )*/
    )
    );
  }
}
